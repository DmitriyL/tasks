import math


class Planet:
    planet_distances = []

    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def add_distant(self, p):
        self.planet_distances.append(math.sqrt((self.x-p.x) * (self.x-p.x)
                                               + (self.y-p.y) * (self.y-p.y)
                                               + (self.z-p.z) * (self.z-p.z)))


a = int(input())
b = input().split(' ')
planets = []
for i in range(0, a):
    c = input().split(' ')
    print(c)
    p = Planet(int(c[0]), int(c[1]), int(c[2]))
    planets.append(p)

print(planets[0].planet_distances)
for i in range(0, a):
    for j in range(0, a):
        planets[i].add_distant(planets[j])

for i in range(0, a):
    print(planets[i].planet_distances)
