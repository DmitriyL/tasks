a = input().split(' ')
a = [int(a[0]), int(a[1])]
b = []
d = []
for i in range(0, a[1]+2):
    d.append('0')

b.append(d)
for i in range(0, a[0]):
    c = []
    c.append('0')
    g = input().split(' ')
    for j in range(0, a[1]):
        c.append(g[j])
    c.append('0')
    b.append(c)
b.append(d)


def req(x, y, mas):
    if b[x][y] != '0':
        mas.append(b[x][y])
        b[x][y] = '0'
        if b[x+1][y] != '0':

            req(x+1, y, mas)
        if b[x-1][y] != '0':

            req(x-1, y, mas)
        if b[x][y+1] != '0':

            req(x, y+1, mas)
        if b[x][y-1] != '0':
            req(x, y-1, mas)


m = []

for i in range(0, a[0]):
    for j in range(0, a[1]):
        mas = []
        req(i, j, mas)
        if len(mas) != 0:
            m.append(mas)


max_len = 0
for ms in m:
    if len(ms) != 0:
        max_len = max(max_len, len(ms))

for ms in m:
    if max_len == len(ms):
        print(ms)

