a = input().split(" ")
b = [int(a[0]), int(a[1])]
c = []

for i in range(0, b[0]):
    c.append(input().split(' '))


def count_S(g, h):
    ch1 = 0
    for i in range(g, b[0]):
        if c[i][h] == '1':
            for j in range(h, b[1]):
                if c[i][j] == '1':
                    c[i][j] = '0'
                    ch1 = ch1+1
                else:
                    break
        else:
            break
    return ch1


answers = []
for i in range(1, b[0]):
    for j in range(1, b[1]):
        if c[i][j] == '1':
            answers.append(count_S(i, j))

print(sorted(answers))

