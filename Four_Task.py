import math


class Material_point:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def return_x_y_z(self):
        return [self.x, self.y, self.z]


class Boll(Material_point):
    def __init__(self, x, y, z, d, hole=None):
        super().__init__(x, y, z)
        self.d = d
        self.hole = hole


class Hole(Material_point):
    def __init__(self, x, y, z, d):
        super().__init__(x, y, z)
        self.d = d


quantity_bolls = int(input())
quantity_holes = int(input())
pr_p = input().split(' ')
gun = Material_point(int(pr_p[0]), int(pr_p[1]), int(pr_p[2]))
holes = []
bolls = []
for i in range(0, quantity_holes):
    pr_p = input().split(' ')
    hole = Hole(int(pr_p[0]), int(pr_p[1]), int(pr_p[2]), int(pr_p[3]))
    holes.append(hole)
for i in range(0, quantity_bolls):
    pr_p = input().split(' ')
    boll = Boll(int(pr_p[0]), int(pr_p[1]), int(pr_p[2]), int(pr_p[3]))
    bolls.append(boll)
ch = 0
for boll in bolls:
    for hole in holes:
        if (boll.x / hole.x == boll.y / hole.y == boll.z / hole.z) 
            and (100 * math.tan(boll.d / 2) <= hole.d):
            boll.hole = hole
            ch += 1

print(ch)
